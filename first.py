import pandas as pd
from pandas.io.json import json_normalize
import numpy as np
from fbprophet import Prophet
import json
from datetime import datetime

df = []
with open('./data/btce_eth_3month.json') as data_file:
    data = json.load(data_file)
    for item in data:
        date = datetime.utcfromtimestamp(item['time']).strftime('%Y-%m-%d')
        df.append({'ds': date, 'y': item['close']})

    df = json_normalize(df)
    df = df.drop_duplicates('ds')
    # print(df)

    # df["ds"] = df["ds"].map(lambda t: t).unique()

    # df = df.rename(columns={'time': 'ds','close': 'y'})

    # df.head()

    m = Prophet()
    m.fit(df)

    future = m.make_future_dataframe(periods=30)
    # print(future.tail(10))

    forecast = m.predict(future)
    print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail(30))


    #
#     for item in data:
#         df.append({'sd': item['time'], 'y': item['close']})
#
# print(df)
#
# m = Prophet()
# m.fit(df)

# Python
# df = pd.read_json('./data/btce_eth_3month.json', orient='index', precise_float=True)
# print(df)
# df['y'] = np.log(df['y'])
# df.head()