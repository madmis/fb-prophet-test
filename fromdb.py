from peewee import *
import pandas as pd
import json
from datetime import datetime
from datetime import timedelta
from pandas.io.json import json_normalize
from fbprophet import Prophet
import numpy as np
import time
import sys

np.seterr(all='ignore')
DB_PATH = '/home/dmac/www/py/bitbot/var/data/ticker.db'
database = SqliteDatabase(DB_PATH)

class BaseModel(Model):
    class Meta:
        database = database


class Ticker(BaseModel):
    pair = CharField(max_length=10)
    high = DecimalField(max_digits=15, decimal_places=10)
    low = DecimalField(max_digits=15, decimal_places=10)
    avg = DecimalField(max_digits=15, decimal_places=10)
    last = DecimalField(max_digits=15, decimal_places=10)
    buy = DecimalField(max_digits=15, decimal_places=10)
    sell = DecimalField(max_digits=15, decimal_places=10)
    vol = DecimalField(max_digits=15, decimal_places=5)
    vol_cur = DecimalField(max_digits=15, decimal_places=5)
    updated = DateTimeField(index=True)
    updated_timestamp = TimestampField(utc=True, index=True)

    class Meta:
        order_by = ('-updated_timestamp',)
        indexes = (
            # create a unique
            (('updated_timestamp', 'pair'), True),
        )

df = []

result = Ticker.select()\
        .where((Ticker.pair == 'eth_usd') & (Ticker.updated > '2017-06-09 00:00:00')) \
        .order_by(Ticker.updated)
c = result.count()

today = datetime.today()
startdate = today - timedelta(c)

i = 0
for tick in result:
    # print(tick.updated, tick.sell)
    date = startdate + timedelta(i)
    i += 1
    print(date.strftime('%Y-%m-%d'), int(tick.sell))
    df.append({'ds': date.strftime('%Y-%m-%d'), 'y': int(tick.sell)})
    # df.append({'ds': tick.updated, 'y': int(tick.sell)})

df = json_normalize(df)

m = Prophet()
m.fit(df)

future = m.make_future_dataframe(periods=50)
# print(future.tail(10))

forecast = m.predict(future)
print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail(50))

print(m.plot(forecast).show())
time.sleep(60)
