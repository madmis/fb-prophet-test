import pandas as pd
import json
from datetime import datetime
from pandas.io.json import json_normalize
from fbprophet import Prophet
import numpy as np
import time
import sys

np.seterr(all='ignore')

df = []
with open(sys.argv[1]) as data_file:
    data = json.load(data_file)
    for item in data:
        date = datetime.utcfromtimestamp(item[0]).strftime('%Y-%m-%d')
        print(date, item[4])
        df.append({'ds': date, 'y': item[7]})

    df = json_normalize(df)
    df = df.drop_duplicates('ds')

    m = Prophet()
    m.fit(df)

    future = m.make_future_dataframe(periods=10)
    # print(future.tail(10))

    forecast = m.predict(future)
    print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail(20))

    print(m.plot(forecast).show())
    time.sleep(60)

exit()

# chunksize = 10 ** 8
chunksize = 200
for chunk in pd.read_csv('./data/btceUSD.csv', chunksize=chunksize):
    # print(chunk)
    for row in chunk.iterrows():
        print(row[0])
        date = datetime.utcfromtimestamp(row[1].values[0]).strftime('%Y-%m-%d %H:%M:%S')
        print(date)
        print(row[1].values[0], row[1].values[1], row[1].values[2])
        # print(row[2])
        # print(row[3])
    # print(next(chunk.iterrows())[1])
    exit()
